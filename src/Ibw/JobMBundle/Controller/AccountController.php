<?php

namespace Ibw\JobMBundle\Controller;

use Ibw\JobMBundle\Entity\Role;
use Ibw\JobMBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Ibw\JobMBundle\Form\RegistrationType;
use Symfony\Component\Security\Core\Encoder;

class AccountController extends Controller
{
    public function registerAction()
    {

        $userEntity = new User();

        $form = $this->createForm(new RegistrationType(), $userEntity, array(
            'action' => $this->generateUrl('account_create'),
        ));

        return $this->render(
            'IbwJobMBundle:Account:register.html.twig',
            array('form' => $form->createView())
        );
    }

    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new RegistrationType(), new User());
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $role = new Role();
                $role->setUser($user)
                    ->setRole('ROLE_USER');

                $user->addRole($role);

            /** @var Encoder\EncoderFactory $factory */
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);

            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('login'));

        }

        return $this->render(
            'IbwJobMBundle:Account:register.html.twig',
            array('form' => $form->createView())
        );
    }



}