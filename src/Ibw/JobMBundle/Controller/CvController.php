<?php

namespace Ibw\JobMBundle\Controller;

use Ibw\JobMBundle\Entity\Cv;
use Ibw\JobMBundle\Form\CvNameType;
use Ibw\JobMBundle\Form\CvType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder;
use Symfony\Component\HttpFoundation\Response;

class CvController extends Controller
{

    public function indexCvAction()
    {
        $request = $this->getRequest();

        if($request->get('_route') == 'IbwJobMBundle_nonlocalized') {
            return $this->redirect($this->generateUrl('ibw_job_m_homepage'));
        }

        return $this->render('IbwJobMBundle:Cv:index_cv.html.twig'


        );
    }

    public function createCvAction(Request $request)
    {
        $entity  = new Cv();

        $form = $this->createForm(new CvType(),$entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUser($this->get('security.context')->getToken()->getUser());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl(
                'ibw_cv_show_by_user'
            ));
        }

        return $this->render('IbwJobMBundle:Cv:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    public function newCvAction()
    {
//        $nameCv = $_POST['title'];
//        echo $nameCv;
        $entity = new Cv();

        $form = $this->createForm(new CvType(), $entity);
        return $this->render('IbwJobMBundle:Cv:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
//            'nameCv'  => $nameCv
        ));
    }



    public function showCvByUserAction()
    {
        $id = $this->get('security.context')->getToken()->getUser()->getId();

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('IbwJobMBundle:Cv')->getCvByUser($id);

        return $this->render('IbwJobMBundle:Cv:show_cv_by_user.html.twig', array(
            'user'  => $user,
        ));
    }

    public function downloadCvPdfAction($token)
    {

        $em = $this->getDoctrine()->getManager();
        $cvInfo = $em->getRepository('IbwJobMBundle:Cv')->getCvByToken($token);

        $filename = "export_".date("Y_m_d_His").".pdf";

        $html = $this->renderView('IbwJobMBundle:Cv:download_cv.html.twig', array(
            'cvInfo' => $cvInfo
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename='.$filename
            )
        );

    }

    public function downloadCvDocAction($token)
    {
        $em = $this->getDoctrine()->getManager();
        $cvInfo = $em->getRepository('IbwJobMBundle:Cv')->getCvByToken($token);

        $filename = "export_".date("Y_m_d_His").".doc";

        $response = $this->render('IbwJobMBundle:Cv:download_cv.html.twig', array(
            'cvInfo' => $cvInfo
        ));
        $response->headers->set('Content-Type', 'application/msword');

        $response->headers->set('Content-Disposition', 'attachment; filename='.$filename);
        return $response;
    }

    public function downloadCvHtmlAction($token){

        $em = $this->getDoctrine()->getManager();
        $cvInfo = $em->getRepository('IbwJobMBundle:Cv')->getCvByToken($token);

        $filename = "export_".date("Y_m_d_His").".html";

        $response = $this->render('IbwJobMBundle:Cv:download_cv.html.twig', array(
            'cvInfo' => $cvInfo
        ));
        $response->headers->set('Content-Type', 'text/html');

        $response->headers->set('Content-Disposition', 'attachment; filename='.$filename);
        return $response;

    }

    /**
     * Displays a form to edit an existing Cv entity.
     *
     */
    public function editAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);

        $editForm = $this->createForm(new CvType(), $entity);
        $deleteForm = $this->createDeleteForm($token);

        return $this->render('IbwJobMBundle:Cv:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function renameAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);
        $renameForm = $this->createForm(new CvNameType(), $entity);
        return $this->render('IbwJobMBundle:Cv:rename.html.twig', array(
            'entity' => $entity,
            'rename_form' => $renameForm->createView(),
        ));

    }

    public function previewCvAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        $resume = $em->getRepository('IbwJobMBundle:Cv')->getCvByToken($token);

        return $this->render('IbwJobMBundle:Cv:preview_after_sharing.html.twig', array(
            'resume' => $resume
        ));
    }

    private function createDeleteForm($token)
    {
        return $this->createFormBuilder(array
        ('token' => $token

        ))
            ->add('token', 'hidden')
            ->getForm();
    }


    /**
     * Edits an existing Cv entity.
     *
     */
    public function updateAction(Request $request, $token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cv entity.');
        }

        $editForm = $this->createForm(new CvType(), $entity);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ibw_cv_edit', array('token' => $token)));
        }

        return $this->redirect($this->generateUrl('ibw_cv_show_by_user', array(
            'token' => $entity->getToken(),

        )));
    }

    public function updateRenameAction(Request $request, $token)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);

        $renameForm = $this->createForm(new CvNameType(), $entity);

        $renameForm->handleRequest($request);

        if ($renameForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ibw_cv_show_by_user', array('token' => $token)));
        }

        return $this->redirect($this->generateUrl('ibw_cv_rename', array(
            'token' => $entity->getToken(),

        )));
    }

    public function deleteAction($token)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('ibw_cv_show_by_user'));
    }

    public function CopyCvAction($token)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IbwJobMBundle:Cv')->findOneByToken($token);

        $cloneEntity = clone $entity;
        $em->persist($cloneEntity);
        $em->flush();

        return $this->redirect($this->generateUrl('ibw_cv_show_by_user'));

    }

}