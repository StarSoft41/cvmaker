<?php

namespace Ibw\JobMBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * CityRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CityRepository extends EntityRepository
{

    public function getCityName()
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT n FROM IbwJobMBundle:City n'
        );

        return $query->getResult();
    }

}



