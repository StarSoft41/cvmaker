<?php


namespace Ibw\JobMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;




/**
 * City
 */
class City
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name_city;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cv;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    public function setNameCity($name_city)
    {
        $this->name_city = $name_city;

        return $this;
    }

    /**
     * Get name_city
     *
     * @return string
     */
    public function getNameCity()
    {
        return $this->name_city;
    }



    public function __toString()
    {
        return $this->name_city;
    }

    public function __construct()
    {
        $this->cv = new ArrayCollection();
    }

}
