<?php

namespace Ibw\JobMBundle\Form;

use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array(
                'attr' => array(
                    'class' => 'form-control',
//                    'placeholder' => 'Numele',

                ),
                'error_bubbling'=>true,
                'constraints' => array(
                    new Length(array('min' => 5, 'minMessage' => 'Numele trebuie sa aiba cel puțin 5 caractere')),
                    new NotBlank(array('message' => 'Acest camp nu poate fi gol'))
                ),

            ))
            ->add('email', 'email', array(
                'attr' => array(
                    'class' => 'form-control',
//                    'placeholder' => 'Adresa de email',
                ),
                'constraints' => array(
                    new Length(array('min' => 8,'minMessage' => 'Adresa ta de email trebuie să fie de cel puțin 8 caractere')),
                    new Email(array('message' => 'Aceasta valoare nu este o adresa de email')),
                ),
            ))

            ->add('password', 'repeated', array(
                'first_name' => 'password',
                'second_name' => 'confirm',
                'type' => 'password',
                'invalid_message' => 'Parolele nu se potrivesc',
                'error_bubbling'=>true,
                'constraints' => array(
                    new Length(array('min' => 6,'minMessage' => 'Parola ta trebuie să aibă cel puțin 6 caractere'))
                ),
                'first_options' => array(
                    'attr' => array(
                        'class' => 'form-control',
//                        'placeholder' => 'Parola',
                    ),
                    'label' => false
                ),

                'second_options' => array(
                    'attr' => array(
                        'class' => 'form-control',
//                        'placeholder' => 'Confirma Parola'
                    ),
                    'label' => false
                ),
            ));
//            ->add('Register', 'submit', array(
//                'attr' => array(
//                    'class' => 'btn btn-submit submit-button-singIn')
//            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ibw\JobMBundle\Entity\User',
            'attr' => ['id' => 'registration']
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'registration';
    }

}
