<?php

namespace Ibw\JobMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
class CvType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('other_information_work', 'textarea',array(
                'required' => false,
                'label'  => 'form.other_information_work'
            ))
            ->add('qualifications', 'textarea', array(
                'required' => false,
                'label'  => 'form.qualifications'
            ))
            ->add('other_information_education', 'textarea', array(
                'required' => false,
                'label'  => 'form.other_information_education'
            ))
            ->add('interests', 'textarea', array(
                'required' => false,
                'label'  => 'form.interests'
            ))
            ->add('competence', 'textarea', array(
                'required' => false,
                'label'  => 'form.competence'
            ))
            ->add('file', 'file', array('label' => 'form.file', 'required' => false)
            )
//            ->add('is_public', null, array('label' => 'Public?'))
            ->add('city')
            ->add('name', 'text', array( 'label' => 'form.name'))
            ->add('cv_name','text', array( 'label' => 'form.cv_name'))
            ->add('websites','text', array( 'label' => 'form.websites'))
            ->add('street','text', array( 'label' => 'form.street'))
            ->add('country','text', array( 'label' => 'form.country'))
            ->add('job_title','text', array( 'label' => 'form.job_title'))
            ->add('company_name','text', array( 'label' => 'form.company_name'))
            ->add('start_date_work','text', array( 'label' => 'form.start_date_work'))
            ->add('end_date_work','text', array( 'label' => 'form.end_date_work'))
            ->add('specialization','text', array( 'label' => 'form.specialization'))
            ->add('institution_name','text', array( 'label' => 'form.institution_name'))
            ->add('start_date_learn','text', array( 'label' => 'form.start_date_learn'))
            ->add('end_date_learn','text', array( 'label' => 'form.end_date_learn'))
            ->add('phone','text', array( 'label' => 'form.phone'))
            ->add('email', 'email',array('label' => 'form.email'))
            ->add('new_section_1', 'textarea', array(
                'required' => false,
                'label' => 'form.new_section_1'
            ))
            ->add('new_section_2', 'textarea', array(
                'required' => false,
                'label' => 'form.new_section_2'
            ))
            ->add('new_section_3', 'textarea', array(
                'required' => false,
                'label' => 'form.new_section_3'
            ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ibw\JobMBundle\Entity\Cv',
        ));
    }


    public function getName()
    {
        return 'cv';
    }

}



