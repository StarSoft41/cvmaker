execute "update composer.phar" do
    cwd release_path
    command "curl -s http://getcomposer.org/installer | php"
    action :run
end

execute "install composer.json (actually composer.lock)" do
    cwd release_path
    command "php composer.phar install --prefer-dist"
    user "deploy"
    action :run
end

execute "prepare symfony cache for production" do
    cwd release_path
    command "php app/console cache:clear --env=prod"
    action :run
end

execute "fix cache and logs owner" do
    cwd release_path
    command "chown -R www-data:www-data app/cache app/logs tmp"
    action :run
end

execute "fix cache and logs permissions" do
    cwd release_path
    command "chmod -R 777 app/cache app/logs tmp"
    action :run
end
